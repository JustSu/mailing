from datetime import datetime, timedelta

from fastapi import status
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool

from database import Base
from main import app, get_db

SQLALCHEMY_DATABASE_URL = 'sqlite://'
engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={'check_same_thread': False},
    poolclass=StaticPool,
)
TestingSessionLocal = sessionmaker(
    autocommit=False, autoflush=False, bind=engine
)
Base.metadata.create_all(bind=engine)


def override_get_db():
    """Creates datebase session."""
    try:
        db = TestingSessionLocal()
        yield db
    except:
        db.close()


app.dependency_overrides[get_db] = override_get_db
client = TestClient(app)


def test_client_create():
    """Test for creating a new client."""
    data = {"phone": "9269802430", "code": "+7", "teg": "some", "utc": 3}
    request = client.post('/users/create/', json=data)
    assert request.status_code == status.HTTP_201_CREATED
    assert request.json() == data


def test_client_update():
    """Test for updating the client."""
    valid_request = client.patch('/users/update/1/', json={'teg': 'foo'})
    invalid_request = client.patch('/users/update/2/', json={'teg': 'foo'})
    assert valid_request.status_code == status.HTTP_200_OK
    assert valid_request.json()['teg'] == 'foo'
    assert invalid_request.status_code == status.HTTP_404_NOT_FOUND
    assert invalid_request.json() == {'Error': 'The object not found'}


def test_mailing_create():
    """Test for creating a new mailing."""
    valid_request = client.post(
        '/mailing/create/', json={'message': 'Test message'}
    )
    invalid_request = client.post(
        '/mailing/create/',
        json={
            'message': 'Invalid mailing',
            'filter': 'foo',
            'end': str(datetime.now() - timedelta(minutes=10)),
        },
    )
    assert valid_request.status_code == status.HTTP_201_CREATED
    assert valid_request.json()['message'] == 'Test message'
    assert invalid_request.status_code == status.HTTP_400_BAD_REQUEST
    assert invalid_request.json() == {
        'Error': 'The end time has already passed'
    }


def test_mailing_update():
    """Test for updating the mailing."""
    valid_request = client.patch(
        '/mailing/update/1/', json={'message': 'updated mailing'}
    )
    invalid_request = client.patch(
        '/mailing/update/2/', json={'message': 'invalid request'}
    )
    assert valid_request.status_code == status.HTTP_200_OK
    assert valid_request.json()['message'] == 'updated mailing'
    assert invalid_request.status_code == status.HTTP_404_NOT_FOUND
    assert invalid_request.json() == {'Error': 'The object not found'}


def test_get_detail_statistics():
    """Test for geting detail statistics."""
    valid_request = client.get('/statistics/1/')
    invalid_request = client.get('/statistics/2/')
    assert valid_request.status_code == status.HTTP_200_OK
    assert valid_request.json() == {"updated mailing": []}
    assert invalid_request.status_code == status.HTTP_404_NOT_FOUND
    assert invalid_request.json() == {'Error': 'The object not found'}


def test_get_full_statistics():
    """Test for geting full statistics."""
    request = client.get('/statistics/')
    assert request.status_code == status.HTTP_200_OK
    assert request.json() == {
        "updated mailing": {"Status OK": 0, "Status FAIL": 0}
    }


def test_client_delete():
    """Checking for client deletion."""
    valid_request = client.delete('/users/delete/1/')
    invalid_request = client.delete('/users/delete/1/')
    assert valid_request.status_code == status.HTTP_204_NO_CONTENT
    assert valid_request.json() == {'Action': 'The object deleted'}
    assert invalid_request.status_code == status.HTTP_404_NOT_FOUND
    assert invalid_request.json() == {'Error': 'The object not found'}


def test_mailing_delete():
    """Checking for mailing deletion."""
    valid_request = client.delete('/mailing/delete/1/')
    invalid_request = client.delete('/mailing/delete/1/')
    assert valid_request.status_code == status.HTTP_204_NO_CONTENT
    assert valid_request.json() == {'Action': 'The object deleted'}
    assert invalid_request.status_code == status.HTTP_404_NOT_FOUND
    assert invalid_request.json() == {'Error': 'The object not found'}
