from datetime import datetime, timedelta

from pydantic import BaseModel, Field


class ShemaMailing(BaseModel):
    start: datetime | None = datetime.now()
    message: str | None = None
    filter: str | None = None
    end: datetime | None = datetime.now() + timedelta(hours=2)


class ShemaClient(BaseModel):
    phone: str | None = Field(default=None, pattern=r'^\d{10}$')
    code: str | None = Field(default=None, pattern=r'^\+\d+$')
    teg: str | None = 'empty'
    utc: int | None = None

    class Config:
        from_attributes = True


class ShemaMessage(BaseModel):
    status: str
    mailing_id: int
    client_id: int
