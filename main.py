import asyncio
import logging
from datetime import datetime

import requests
from fastapi import Depends, FastAPI, Response, status
from fastapi.responses import JSONResponse
from sqlalchemy.orm import Session

from database import Base, SessionLocal, engine
from models import ModelClient, ModelMailing, ModelMessage
from shemas import ShemaClient, ShemaMailing

app = FastAPI(swagger_ui_parameters=({"syntaxHighlight.theme": "obsidian"}))
Base.metadata.create_all(bind=engine)


async def get_db():
    """Creates a database session."""
    db = SessionLocal()
    try:
        yield db
    except:
        db.close()


logging.basicConfig(
    level=logging.INFO,
    filename='./info.log',
    format='%(asctime)s %(levelname)s: %(message)s',
)


@app.post('/users/create/', response_model=ShemaClient)
async def create_client(
    client: ShemaClient, response: Response, db: Session = Depends(get_db)
):
    """Creates a new client."""
    client = ModelClient(
        phone=str(client.phone),
        code=str(client.code),
        teg=client.teg,
        utc=client.utc,
    )
    db.add(client)
    db.commit()
    logging.info(
        f'Create a new client: [id: {client.id}, phone: {client.phone}]'
    )
    response.status_code = status.HTTP_201_CREATED
    return client


@app.patch('/users/update/{user_id}/', response_model=ShemaClient)
async def update_client(
    data: ShemaClient, user_id: int, db: Session = Depends(get_db)
):
    """Updates the client."""
    try:
        client = db.query(ModelClient).filter(ModelClient.id == user_id).one()
        client.phone = data.phone or client.phone
        client.code = data.code or client.code
        client.teg = data.teg or client.teg
        client.utc = data.utc or client.utc
        db.add(client)
        db.commit()
        logging.info(f'The client with ID: {user_id} has been updated')
        return client
    except:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={'Error': 'The object not found'},
        )


@app.delete('/users/delete/{user_id}/')
async def delete_client(user_id: int, db: Session = Depends(get_db)):
    """Deletes the client."""
    try:
        obj = db.query(ModelClient).filter(ModelClient.id == user_id).one()
        db.delete(obj)
        db.commit()
        logging.info(f'The client with ID: {user_id} has been deleted')
        return JSONResponse(
            status_code=status.HTTP_204_NO_CONTENT,
            content={'Action': 'The object deleted'},
        )
    except:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={'Error': 'The object not found'},
        )


@app.post('/mailing/create/', response_model=ShemaMailing)
async def create_mailing(
    mailing: ShemaMailing, response: Response, db: Session = Depends(get_db)
):
    """Creates a new mailing and starts send messages to clients."""
    new_mailing = ModelMailing(
        start=mailing.start,
        message=mailing.message,
        filter=mailing.filter,
        end=mailing.end,
    )
    if new_mailing.end <= datetime.now():
        return JSONResponse(
            status_code=status.HTTP_400_BAD_REQUEST,
            content={'Error': 'The end time has already passed'},
        )
    db.add(new_mailing)
    db.commit()
    logging.info(f'The mailing with ID: {new_mailing.id} was been created')
    result = await queue(new_mailing, db)
    response.status_code = status.HTTP_201_CREATED
    return result


@app.patch('/mailing/update/{mailing_id}/', response_model=ShemaMailing)
async def update_mailing(
    data: ShemaMailing, mailing_id: int, db: Session = Depends(get_db)
):
    """Updates the mailing."""
    try:
        mailing = (
            db.query(ModelMailing).filter(ModelMailing.id == mailing_id).one()
        )
        mailing.start = data.start or mailing.start
        mailing.message = data.message or mailing.message
        mailing.filter = data.filter or mailing.filter
        mailing.end = data.end or mailing.end
        db.add(mailing)
        db.commit()
        logging.info(f'The mailing with ID: {mailing_id} has been updated')
        return mailing
    except:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={'Error': 'The object not found'},
        )


@app.delete('/mailing/delete/{mailing_id}/')
async def delete_mailing(mailing_id: int, db: Session = Depends(get_db)):
    """Deletes the mailing."""
    try:
        obj = (
            db.query(ModelMailing).filter(ModelMailing.id == mailing_id).one()
        )
        db.delete(obj)
        db.commit()
        logging.info(f'The mailing with ID: {mailing_id} has been deleted')
        return JSONResponse(
            status_code=status.HTTP_204_NO_CONTENT,
            content={'Action': 'The object deleted'},
        )
    except:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={'Error': 'The object not found'},
        )


@app.get('/statistics/{pk}/')
async def get_detail_statistics(pk: int, db: Session = Depends(get_db)):
    """Receives detailed statistics on the mailing."""
    try:
        data = db.query(ModelMailing).filter(ModelMailing.id == pk).one()
        response = {data.message: data.mailing}
        logging.info(f'Collected statistics for mailing with ID: {pk}')
        return JSONResponse(status_code=status.HTTP_200_OK, content=response)
    except:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={'Error': 'The object not found'},
        )


@app.get('/statistics/')
async def get_statistics(db: Session = Depends(get_db)):
    """Receives full statistics on all mailings."""
    data = db.query(ModelMailing)
    response = dict()
    for item in data:
        response[item.message] = {'Status OK': 0, 'Status FAIL': 0}
        for i in item.mailing:
            if i.status == 'ok':
                response[item.message]['Status OK'] += 1
            elif i.status == 'fail':
                response[item.message]['Status FAIL'] += 1
            else:
                return JSONResponse(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    content={'Error': 'Got invalid data'},
                )
    logging.info('General statistics on all mailings have been collected')
    return JSONResponse(status_code=status.HTTP_200_OK, content=response)


async def queue(mailing: ModelMailing, db: Session):
    """Queue tasks. Sends messages at the right time."""
    while mailing.start >= datetime.now():
        waiting = (mailing.start - datetime.now()).seconds
        await asyncio.sleep(waiting)
    else:
        logging.info(f'Mailing with ID: {mailing.id} in progress')
        clients = get_clients(mailing, db)
        logging.info('Sending messages has started')
        send_message(clients, mailing, db)
        logging.info('Sending messages has finished')
        return mailing


def get_clients(mailing: ModelMailing, db: Session):
    """Gets a list of required clients."""
    clients = (
        db.query(ModelClient)
        .filter(
            (ModelClient.code == mailing.filter)
            | (ModelClient.teg == mailing.filter)
        )
        .all()
    )
    logging.info(f'Got a list of clients for mailing with ID: {mailing.id}')
    return clients


def check_result(response):
    """Checks the response data."""
    data = response.json()
    status = response.status_code == 200
    code = data['code'] == 0
    message = data['message'] == 'OK'
    status = 'ok' if status and code and message else 'fail'
    logging.info(f'Status: {data["message"]}, code: {response.status_code}')
    return status


def send_message(
    clients: list[ModelClient], mailing: ModelMailing, db: Session
):
    """Sends messages to clients."""
    auth_token = (
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3M'
        'jE4MTYzOTgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzO'
        'i8vdC5tZS9CYXJhYmluUm9tYW4ifQ.Koca8qtg6feb5EKQeKisC'
        'sb0OD9KWxrgM-rV_WY0vbQ'
    )
    headers = {'Authorization': f'Bearer {auth_token}'}
    for client in clients:
        data = {
            'id': int(mailing.id),
            'phone': int(client.phone),
            'text': mailing.message,
        }
        response = requests.post(
            f'https://probe.fbrq.cloud/v1/send/{mailing.id}',
            json=data,
            headers=headers,
        )
        status = check_result(response)
        obj = ModelMessage(
            status=status, mailing_id=mailing.id, client_id=client.id
        )
        db.add(obj)
        logging.info(f'Send message: {mailing.id} to client: {client.id}')
    db.commit()
