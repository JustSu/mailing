from datetime import datetime, timedelta

from sqlalchemy import (Column, DateTime, ForeignKey, Identity, Integer,
                        String, Text)
from sqlalchemy.orm import relationship

from database import Base


class ModelMailing(Base):
    __tablename__ = 'mailing'
    id = Column(
        Integer, Identity(start=1, cycle=True), primary_key=True, index=True
    )
    start = Column(DateTime, default=datetime.now)
    message = Column(Text, nullable=False)
    filter = Column(String(100))
    end = Column(DateTime, default=(datetime.now() + timedelta(hours=2)))


class ModelClient(Base):
    __tablename__ = 'clients'
    id = Column(
        Integer, Identity(start=1, cycle=True), primary_key=True, index=True
    )
    phone = Column(String(10), nullable=False)
    code = Column(String(10), nullable=False)
    teg = Column(String(100))
    utc = Column(Integer, nullable=False)


class ModelMessage(Base):
    __tablename__ = 'message'
    id = Column(
        Integer, Identity(start=1, cycle=True), primary_key=True, index=True
    )
    date = Column(DateTime, default=datetime.now)
    status = Column(String(100), nullable=False)
    mailing_id = Column(ForeignKey('mailing.id'))
    client_id = Column(ForeignKey('clients.id'))

    mailing = relationship('ModelMailing', backref='mailing')
    client = relationship('ModelClient', backref='client')
